# electricrailmap.co.uk

Source for static site 

## Local Dev

Assume python3 in installed...

```bash
git clone git@gitlab.com:fizzy-knitting/electricrailmap.co.uk.git
cd electricrailmap.co.uk
pip3 install requirements.txt
mkdocs serve

```

then open browser at 